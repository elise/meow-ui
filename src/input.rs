use alloc::vec::Vec;
use nx::{
    input::Player,
    ipc::sf::hid::{NpadAttribute, NpadStyleTag},
    result::Result,
    service::hid::{NpadButton, NpadIdType},
};

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum InputEvent {
    LeftJoystickLeft,
    LeftJoystickRight,
    LeftJoystickUp,
    LeftJoystickDown,
    LeftJoystickClick,

    RightJoystickLeft,
    RightJoystickRight,
    RightJoystickUp,
    RightJoystickDown,
    RightJoystickClick,

    Left,
    Right,
    Up,
    Down,

    X,
    Y,
    A,
    B,

    L,
    R,
    ZL,
    ZR,

    Minus,
    Plus,
}

macro_rules! check_input {
    ($input:ident, $output:ident, [$($inval:expr => $outval:expr),*]) => {
        $(if $input.contains($inval) {
            $output.push($outval);
        })*
    };
}

impl InputEvent {
    pub fn collect_events(input: NpadButton, output: &mut Vec<Self>) {
        check_input!(input, output, [
            NpadButton::StickLLeft() => Self::LeftJoystickLeft,
            NpadButton::StickLRight() => Self::LeftJoystickRight,
            NpadButton::StickLUp() => Self::LeftJoystickUp,
            NpadButton::StickLDown() => Self::LeftJoystickDown,
            NpadButton::StickL() => Self::LeftJoystickClick,

            NpadButton::StickRLeft() => Self::RightJoystickLeft,
            NpadButton::StickRRight() => Self::RightJoystickRight,
            NpadButton::StickRUp() => Self::RightJoystickUp,
            NpadButton::StickRDown() => Self::RightJoystickDown,
            NpadButton::StickR() => Self::RightJoystickClick,

            NpadButton::Left() => Self::Left,
            NpadButton::Right() => Self::Right,
            NpadButton::Up() => Self::Up,
            NpadButton::Down() => Self::Down,

            NpadButton::X() => Self::X,
            NpadButton::Y() => Self::Y,
            NpadButton::A() => Self::A,
            NpadButton::B() => Self::B,

            NpadButton::L() => Self::L,
            NpadButton::R() => Self::R,
            NpadButton::ZL() => Self::ZL,
            NpadButton::ZR() => Self::ZR,

            NpadButton::Minus() => Self::Minus,
            NpadButton::Plus() => Self::Plus
        ]);
    }
    
    pub fn collect_events_left(input: NpadButton, output: &mut Vec<Self>) {
        check_input!(input, output, [
            NpadButton::StickLLeft() => Self::LeftJoystickDown,
            NpadButton::StickLRight() => Self::LeftJoystickUp,
            NpadButton::StickLUp() => Self::LeftJoystickLeft,
            NpadButton::StickLDown() => Self::LeftJoystickRight,
            NpadButton::StickL() => Self::LeftJoystickClick,
            
            NpadButton::Left() => Self::B,
            NpadButton::Right() => Self::X,
            NpadButton::Up() => Self::Y,
            NpadButton::Down() => Self::A,
            
            NpadButton::LeftSL() => Self::L,
            NpadButton::LeftSR() => Self::R,
            
            NpadButton::Minus() => Self::Minus
        ]);
    }
    
    pub fn collect_events_right(input: NpadButton, output: &mut Vec<Self>) {
        check_input!(input, output, [
            NpadButton::StickRLeft() => Self::LeftJoystickUp,
            NpadButton::StickRRight() => Self::LeftJoystickDown,
            NpadButton::StickRUp() => Self::LeftJoystickRight,
            NpadButton::StickRDown() => Self::LeftJoystickLeft,
            NpadButton::StickR() => Self::LeftJoystickClick,
            
            NpadButton::X() => Self::A,
            NpadButton::Y() => Self::X,
            NpadButton::A() => Self::B,
            NpadButton::B() => Self::Y,
            
            NpadButton::RightSL() => Self::L,
            NpadButton::RightSR() => Self::R,
            
            NpadButton::Plus() => Self::Plus
        ]);
    }
}

pub struct InputContext {
    _ctx: nx::input::Context,
    player_handheld: Player,
    player_n1: Player,
}

macro_rules! have_style_active {
    ($self:ident,$style:expr) => {
        $self.player_n1.get_supported_style_tags().contains($style) && $self.player_n1.get_style_tag_attributes($style).contains(NpadAttribute::IsConnected())
    };
}

macro_rules! poll_buttons {
    ($self:ident,$direction:ident) => {{
        let mut out = Vec::new();
        // First, deal with handheld mode. this is fairly simple
        if $self.player_handheld.get_supported_style_tags().contains(NpadStyleTag::Handheld()) {
            InputEvent::collect_events($self.player_handheld.$direction(NpadStyleTag::Handheld()), &mut out);
        }

        // Next, deal with if we have a full key controller or dual joycon
        for test_style in [NpadStyleTag::JoyDual(), NpadStyleTag::FullKey()] {
            if have_style_active!($self, test_style) {
                InputEvent::collect_events($self.player_n1.$direction(test_style), &mut out);
            }
        }
        
        // Now, deal with an independent left joycon
        if have_style_active!($self, NpadStyleTag::JoyLeft()) {
            InputEvent::collect_events_left($self.player_n1.$direction(NpadStyleTag::JoyLeft()), &mut out);
        }

        // Aaaaaand finally an independent right joycon
        if have_style_active!($self, NpadStyleTag::JoyRight()) {
            InputEvent::collect_events_right($self.player_n1.$direction(NpadStyleTag::JoyRight()), &mut out);
        }
        
        out
    }};
}

impl InputContext {
    pub fn new() -> Result<Self> {
        let ctx = nx::input::Context::new(
            NpadStyleTag::Handheld() | NpadStyleTag::FullKey() | NpadStyleTag::JoyDual() | NpadStyleTag::JoyLeft() | NpadStyleTag::JoyRight(),
            &[NpadIdType::Handheld, NpadIdType::No1],
        )?;
        Ok(Self {
            player_handheld: ctx.get_player(NpadIdType::Handheld),
            player_n1: ctx.get_player(NpadIdType::No1),
            _ctx: ctx,
        })
    }

    pub fn poll_events_down(&mut self) -> Vec<InputEvent> {
        poll_buttons!(self, get_style_tag_buttons_down)
    }

    pub fn poll_events_up(&mut self) -> Vec<InputEvent> {
        poll_buttons!(self, get_style_tag_buttons_up)
    }
}
