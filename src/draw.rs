use alloc::vec::Vec;
use nx::result::Result;

use crate::colour::Colour;

struct DrawBuffer {
    pub buffer: *mut u32,
    pub size: usize,
}

impl DrawBuffer {
    pub const fn new(buffer: *mut u32, size: usize) -> Self {
        Self { buffer, size }
    }

    pub const fn null() -> Self {
        Self {
            buffer: core::ptr::null_mut(),
            size: 0,
        }
    }

    pub unsafe fn alloc_unchecked(size: usize) -> Self {
        let layout = alloc::alloc::Layout::from_size_align_unchecked(size, 8);
        let buffer = alloc::alloc::alloc_zeroed(layout);

        Self::new(buffer as *mut u32, size)
    }

    pub fn dealloc(&mut self) {
        unsafe {
            let layout = alloc::alloc::Layout::from_size_align_unchecked(self.size, 8);
            alloc::alloc::dealloc(self.buffer as *mut u8, layout);
        }
    }
}

pub struct Draw {
    gpu_buffer: DrawBuffer,
    linear_buffer: DrawBuffer,

    stride: u32,
    width: u32,
    height: u32,

    slot: i32,
    fences: nx::gpu::MultiFence,

    default_font: rusttype::Font<'static>,
}

impl From<&nx::gpu::surface::Surface> for Draw {
    fn from(surface: &nx::gpu::surface::Surface) -> Self {
        let stride = surface.compute_stride();
        let width = surface.get_width();
        let height = surface.get_height();
        let aligned_width = stride as usize;
        let aligned_height = ((height + 7) & !7) as usize;
        let linear_buffer_size = aligned_width * aligned_height;

        let linear_buffer = unsafe { DrawBuffer::alloc_unchecked(linear_buffer_size) };

        let font = rusttype::Font::try_from_bytes(
            include_bytes!("./fonts/Roboto/Roboto-Medium.ttf") as &[u8],
        )
        .unwrap();

        Self {
            gpu_buffer: DrawBuffer::null(),
            linear_buffer,
            stride,
            width,
            height,
            slot: 0,
            fences: unsafe { core::mem::zeroed() },
            default_font: font,
        }
    }
}

impl Draw {
    pub fn start_cycle(&mut self, surface: &mut nx::gpu::surface::Surface) -> Result<()> {
        let (buffer, buffer_size, slot, _, fences) = surface.dequeue_buffer(true)?;
        self.gpu_buffer = DrawBuffer::new(buffer as *mut u32, buffer_size);
        self.slot = slot;
        self.fences = fences;
        surface.wait_fences(fences, -1)
    }

    pub fn end_cycle(&mut self, surface: &mut nx::gpu::surface::Surface) -> Result<()> {
        Self::convert_buffers_impl(
            self.gpu_buffer.buffer as *mut u8,
            self.linear_buffer.buffer as *mut u8,
            self.stride,
            self.height,
        );
        nx::arm::cache_flush(self.gpu_buffer.buffer as *mut u8, self.gpu_buffer.size);
        surface.queue_buffer(self.slot, self.fences)?;
        surface.wait_vsync_event(-1)
    }

    pub fn clear(&mut self, colour: Colour) {
        for idx in 0..(self.linear_buffer.size / core::mem::size_of::<u32>()) {
            unsafe {
                let ptr = self.linear_buffer.buffer.add(idx);
                *ptr = colour.encode();
            }
        }
    }

    pub fn set_pixel(&mut self, x: u32, y: u32, colour: Colour) {
        let idx = ((self.stride / core::mem::size_of::<u32>() as u32) * y + x) as usize;
        unsafe {
            let ptr = self.linear_buffer.buffer.add(idx);
            *ptr = colour.blend(Colour::decode(*ptr)).encode();
        }
    }

    pub fn fill(&mut self, x: i32, y: i32, width: u32, height: u32, colour: Colour) {
        let x0 = x.clamp(0, self.width as i32) as u32;
        let y0 = y.clamp(0, self.height as i32) as u32;
        let x1 = (x + width as i32).clamp(0, self.width as i32) as u32;
        let y1 = (y + height as i32).clamp(0, self.height as i32) as u32;

        for y in y0..y1 {
            for x in x0..x1 {
                self.set_pixel(x, y, colour);
            }
        }
    }

    pub fn text(&mut self, x: i32, y: i32, size: f32, colour: Colour, text: &str) -> (u32, u32) {
        let scale = rusttype::Scale::uniform(size);
        let v_metrics: rusttype::VMetrics = self.default_font.v_metrics(scale);

        let mut highest_x = x.max(0) as u32;
        let mut y_offset = 0u32;
        for line in text.lines() {
            let glyphs = self
                .default_font
                .layout(line, scale, rusttype::point(0.0, v_metrics.ascent))
                .collect::<Vec<_>>();
            for glyph in glyphs {
                if let Some(bbox) = glyph.pixel_bounding_box() {
                    glyph.draw(|g_x, g_y, g_v| {
                        let mut pixel_colour = colour;
                        pixel_colour.a = (g_v * 255.0) as u8;
                        let x = x + g_x as i32 + bbox.min.x;
                        let y = y + y_offset as i32 + g_y as i32 + bbox.min.y;

                        if !x.is_negative() && !y.is_negative() {
                            highest_x = highest_x.max(x as u32);

                            self.set_pixel(x as u32, y as u32, pixel_colour);
                        }
                    });
                }
            }

            y_offset += v_metrics.ascent as u32;
        }

        (highest_x, y_offset)
    }

    fn convert_buffers_gob_impl(out_gob_buf: *mut u8, in_gob_buf: *mut u8, stride: u32) {
        unsafe {
            let mut tmp_out_gob_buf_128 = out_gob_buf as *mut u128;
            for i in 0..32 {
                let y = ((i >> 1) & 0x6) | (i & 0x1);
                let x = ((i << 3) & 0x10) | ((i << 1) & 0x20);
                let out_gob_buf_128 = tmp_out_gob_buf_128 as *mut u128;
                let in_gob_buf_128 = in_gob_buf.offset((y * stride + x) as isize) as *mut u128;
                *out_gob_buf_128 = *in_gob_buf_128;
                tmp_out_gob_buf_128 = tmp_out_gob_buf_128.offset(1);
            }
        }
    }

    fn convert_buffers_impl(out_buf: *mut u8, in_buf: *mut u8, stride: u32, height: u32) {
        let block_height_gobs = 1 << nx::gpu::BLOCK_HEIGHT_LOG2;
        let block_height_px = 8 << nx::gpu::BLOCK_HEIGHT_LOG2;

        let width_blocks = stride >> 6;
        let height_blocks = (height + block_height_px - 1) >> (3 + nx::gpu::BLOCK_HEIGHT_LOG2);
        let mut tmp_out_buf = out_buf;

        for block_y in 0..height_blocks {
            for block_x in 0..width_blocks {
                for gob_y in 0..block_height_gobs {
                    unsafe {
                        let x = block_x * 64;
                        let y = block_y * block_height_px + gob_y * 8;
                        if y < height {
                            let in_gob_buf = in_buf.offset((y * stride + x) as isize);
                            Self::convert_buffers_gob_impl(tmp_out_buf, in_gob_buf, stride);
                        }
                        tmp_out_buf = tmp_out_buf.offset(512);
                    }
                }
            }
        }
    }
}

impl Drop for Draw {
    fn drop(&mut self) {
        self.linear_buffer.dealloc();
        self.linear_buffer = DrawBuffer::null();
    }
}
