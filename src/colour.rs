#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Colour {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Colour {
    pub const BLUE: Self = Self::new(0x1C, 0x9B, 0xF2, 0xFF);
    pub const GREEN: Self = Self::new(0x00, 0xFD, 0xC5, 0xFF);
    pub const DARK_GREY: Self = Self::new(0x50, 0x50, 0x50, 0xFF);
    pub const LIGHT_GREY: Self = Self::new(0x7E, 0x7E, 0x7E, 0xFF);
    pub const BLACK: Self = Self::new(0x2D, 0x2D, 0x2D, 0xFF);
    pub const WHITE: Self = Self::new(0xE0, 0xE0, 0xE0, 0xFF);
    pub const YELLOW: Self = Self::new(0xFF, 0xAE, 0x02, 0xFF);
    pub const RED: Self = Self::new(0xFF, 0x51, 0x31, 0xFF);

    pub const fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self { r, g, b, a }
    }

    pub const fn blend(&self, rhs: Self) -> Self {
        const fn blend_single(lhs: u8, rhs: u8, a: u8) -> u8 {
            (((lhs as u32 * a as u32) + (rhs as u32 * (0xFF - a as u32))) / 0xFF) as u8
        }

        Self::new(
            blend_single(self.r, rhs.r, self.a),
            blend_single(self.g, rhs.g, self.a),
            blend_single(self.b, rhs.b, self.a),
            0xFF,
        )
    }

    pub const fn encode(&self) -> u32 {
        (self.r as u32 & 0xFF)
            | ((self.g as u32 & 0xFF) << 8)
            | ((self.b as u32 & 0xFF) << 16)
            | ((self.a as u32 & 0xFF) << 24)
    }

    pub const fn decode(value: u32) -> Self {
        Self::new(
            value as u8,
            (value >> 8) as u8,
            (value >> 16) as u8,
            (value >> 24) as u8,
        )
    }
}
