use alloc::string::ToString;

use crate::input::InputEvent;

use super::{
    text::{Text, TextSize},
    Callback, Component,
};

pub struct TextButton {
    pub selected: bool,
    pub text: Text,
    x: super::ComponentCoordiate,
    y: super::ComponentCoordiate,
    on_click: Option<Callback>,
    on_select: Option<Callback>,
}

impl TextButton {
    pub fn new(text: &str, x: super::ComponentCoordiate, y: super::ComponentCoordiate) -> Self {
        Self {
            selected: false,
            text: Text::new(text.to_string(), 0, 0, TextSize::Small, false),
            x,
            y,
            on_click: None,
            on_select: None,
        }
    }

    pub fn on_click<T: Into<Callback>>(mut self, handler: T) -> Self {
        self.on_click = Some(handler.into());
        self
    }

    pub fn on_select<T: Into<Callback>>(mut self, handler: T) -> Self {
        self.on_select = Some(handler.into());
        self
    }

    pub fn text_size(mut self, text_size: TextSize) -> Self {
        self.text.size = text_size;
        self
    }
}

impl Component for TextButton {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        self.text.selected = self.selected;

        let text_size = self
            .text
            .draw_frame((parent.0 + self.x, parent.1 + self.y), draw, theme);
        (text_size.0, text_size.1)
    }

    fn selected(&mut self, selected: bool) {
        if !self.selected && selected {
            if let Some(on_select) = self.on_select.as_ref() {
                on_select.call();
            }
        }

        self.selected = selected;
    }

    fn selectable(&self) -> bool {
        true
    }

    fn handle_event(&mut self, event: crate::input::InputEvent) {
        if event == InputEvent::A {
            if let Some(on_click) = self.on_click.as_ref() {
                on_click.call();
            }
        }
    }
}
