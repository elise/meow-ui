use crate::{colour::Colour, draw::Draw, themes::Theme};

use super::{Component, ComponentCoordiate, ComponentSize};

pub struct FilledRectangle {
    pub x: ComponentCoordiate,
    pub y: ComponentCoordiate,
    pub width: ComponentSize,
    pub height: ComponentSize,
    pub foreground: bool,
}

impl FilledRectangle {
    pub fn new(
        x: ComponentCoordiate,
        y: ComponentCoordiate,
        width: ComponentSize,
        height: ComponentSize,
        foreground: bool,
    ) -> Self {
        Self {
            x,
            y,
            width,
            height,
            foreground,
        }
    }
}

impl Component for FilledRectangle {
    fn x(&self) -> ComponentCoordiate {
        self.x
    }

    fn y(&self) -> ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (ComponentCoordiate, ComponentCoordiate),
        draw: &mut Draw,
        theme: &Theme,
    ) -> (ComponentSize, ComponentSize) {
        draw.fill(
            parent.0 + self.x,
            parent.1 + self.y,
            self.width,
            self.height,
            match self.foreground {
                true => theme.foreground,
                false => theme.background,
            },
        );

        (self.width, self.height)
    }
}

pub struct ColouredRectangle {
    pub x: ComponentCoordiate,
    pub y: ComponentCoordiate,
    pub width: ComponentSize,
    pub height: ComponentSize,
    pub colour: Colour,
}

impl ColouredRectangle {
    pub fn new(
        x: ComponentCoordiate,
        y: ComponentCoordiate,
        width: ComponentSize,
        height: ComponentSize,
        colour: Colour,
    ) -> Self {
        Self {
            x,
            y,
            width,
            height,
            colour,
        }
    }
}

impl Component for ColouredRectangle {
    fn x(&self) -> ComponentCoordiate {
        self.x
    }

    fn y(&self) -> ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (ComponentCoordiate, ComponentCoordiate),
        draw: &mut Draw,
        _theme: &Theme,
    ) -> (ComponentSize, ComponentSize) {
        draw.fill(
            parent.0 + self.x,
            parent.1 + self.y,
            self.width,
            self.height,
            self.colour,
        );

        (self.width, self.height)
    }
}
