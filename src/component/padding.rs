use super::{Component, ComponentSize};

pub struct Padding(pub ComponentSize, pub ComponentSize);

impl Component for Padding {
    fn x(&self) -> super::ComponentCoordiate {
        0
    }

    fn y(&self) -> super::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        _parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        _draw: &mut crate::draw::Draw,
        _theme: &crate::themes::Theme,
    ) -> (ComponentSize, ComponentSize) {
        (self.0, self.1)
    }
}
