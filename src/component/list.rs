use alloc::vec::Vec;
use nx::mem::Shared;

use crate::{colour::Colour, input::InputEvent};

use super::{rectangle::ColouredRectangle, Component};

#[macro_export]
macro_rules! list {
    ($max:expr, ($x:expr, $y:expr), [$($child:expr),*]) => {
        {
            let mut list = $crate::component::list::List::new($x, $y, $max);

            $(list.children.push($child);)*

            if list.children.len() != 0 {
                list = list.selected_idx(0);
            }

            list
        }
    };
    [$($child:expr),*] => {
        list!(10, (0, 0), [$($child),*])
    };
}

pub struct List {
    pub x: super::ComponentCoordiate,
    pub y: super::ComponentCoordiate,

    pub max_drawn: usize,

    pub children: Vec<Shared<dyn Component>>,

    draw_start_idx: usize,
    select: ColouredRectangle,
    selected_idx: Option<usize>,
}

impl List {
    pub fn new(
        x: super::ComponentCoordiate,
        y: super::ComponentCoordiate,
        max_drawn: usize,
    ) -> Self {
        Self {
            x,
            y,
            children: Vec::new(),
            select: ColouredRectangle::new(0, 0, 4, 48, Colour::GREEN),
            selected_idx: None,
            max_drawn,
            draw_start_idx: 0,
        }
    }

    pub fn child<T: Component + 'static>(mut self, child: T) -> Self {
        self.children.push(Shared::new(child));
        self
    }

    pub fn selected_idx(mut self, idx: usize) -> Self {
        self.selected_idx = Some(idx);
        self
    }
}

impl Component for List {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        let mut largest_x = 0;
        let mut total_y = 0;
        for (idx, child) in self
            .children
            .iter()
            .enumerate()
            .skip(self.draw_start_idx)
            .take(self.max_drawn)
        {
            if self.selected_idx == Some(idx) {
                self.select.y = total_y as i32;
                self.select
                    .draw_frame((parent.0 + self.x, parent.1 + self.y), draw, theme);
                child.get().selected(true);
            } else {
                child.get().selected(false);
            }

            let child_size = child.get().draw_frame(
                (
                    parent.0 + self.x + 16,
                    parent.1 + self.y + total_y as i32 + 8,
                ),
                draw,
                theme,
            );

            largest_x = largest_x.max(child_size.0);
            total_y += (child_size.1 + 8).max(48);
        }

        (largest_x + 16, total_y)
    }

    fn handle_event(&mut self, event: InputEvent) {
        match event {
            InputEvent::LeftJoystickUp | InputEvent::Up => {
                if let Some(selected) = self.selected_idx {
                    if selected == 0 {
                        return;
                    }

                    if selected == self.draw_start_idx {
                        self.draw_start_idx -= 1
                    }
                }

                if self.selected_idx.unwrap_or_default() > 0 {
                    let mut idx = 0;

                    loop {
                        idx += 1;

                        if self.selected_idx.unwrap_or_default() - (idx - 1) == 0 {
                            idx = 0;
                            break;
                        }

                        match self
                            .children
                            .get(self.selected_idx.unwrap_or_default() - idx)
                        {
                            Some(shared_component) => {
                                if shared_component.get().selectable() {
                                    break;
                                }
                            }
                            None => {
                                idx = 0;
                                break;
                            }
                        }
                    }

                    if self.selected_idx.is_none() {
                        self.selected_idx = Some(0);
                    }

                    *self.selected_idx.as_mut().unwrap() -= idx;
                }
            }
            InputEvent::LeftJoystickDown | InputEvent::Down => {
                if let Some(selected) = self.selected_idx {
                    let bottom_idx = self.draw_start_idx + (self.max_drawn.max(1) - 1);

                    if selected == (self.children.len().max(1) - 1) {
                        return;
                    }

                    if selected == bottom_idx {
                        self.draw_start_idx += 1
                    }
                }

                if self.selected_idx.unwrap_or_default() + 1 < self.children.len()
                    && self.selected_idx.is_some()
                {
                    let mut idx = 0;

                    loop {
                        idx += 1;

                        if self.selected_idx.unwrap_or_default() + idx >= self.children.len() {
                            idx = 0;
                            break;
                        }

                        match self
                            .children
                            .get(self.selected_idx.unwrap_or_default() + idx)
                        {
                            Some(shared_component) => {
                                if shared_component.get().selectable() {
                                    break;
                                }
                            }
                            None => {
                                idx = 0;
                                break;
                            }
                        }
                    }

                    if self.selected_idx.is_none() {
                        self.selected_idx = Some(0);
                    }

                    *self.selected_idx.as_mut().unwrap() += idx;
                }
            }
            event => {
                if let Some(selected) = self.selected_idx.and_then(|idx| self.children.get(idx)) {
                    selected.get().handle_event(event)
                }
            }
        }
    }
}
