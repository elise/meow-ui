use alloc::vec::Vec;
use nx::mem::Shared;

use super::{Component, ComponentCoordiate};

#[macro_export]
macro_rules! vstack {
    (($x:expr, $y:expr), [$($child:expr),*]) => {
        {
            let mut stack = $crate::component::stack::VStack::new($x, $y);

            $(stack.children.push($child);)*

            ::nx::mem::Shared::new(stack)
        }
    };
    [$($child:expr),*] => {
        vstack!((0, 0), [$($child),*])
    };
}

#[macro_export]
macro_rules! hstack {
    (($x:expr, $y:expr), [$($child:expr),*]) => {
        {
            let mut stack = $crate::component::stack::HStack::new($x, $y);

            $(stack.children.push($child);)*

            ::nx::mem::Shared::new(stack)
        }
    };
    [$($child:expr),*] => {
        hstack!((0, 0), [$($child),*])
    };
}

#[macro_export]
macro_rules! zstack {
    (($x:expr, $y:expr), [$($child:expr),*]) => {
        {
            let mut stack = $crate::component::stack::ZStack::new($x, $y);

            $(stack.children.push($child);)*

            ::nx::mem::Shared::new(stack)
        }
    };
    [$($child:expr),*] => {
        zstack!((0, 0), [$($child),*])
    };
}

#[derive(Default, Clone)]
pub struct VStack {
    x: ComponentCoordiate,
    y: ComponentCoordiate,
    pub children: Vec<Shared<dyn Component>>,
}

impl VStack {
    pub fn new(x: ComponentCoordiate, y: ComponentCoordiate) -> Self {
        Self {
            x,
            y,
            children: Vec::new(),
        }
    }

    pub fn child<T: Component + 'static>(mut self, child: Shared<T>) -> Self {
        self.children.push(child);
        self
    }
}

impl Component for VStack {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        let mut largest_x = 0u32;
        let mut y_offset = 0u32;
        for child in &self.children {
            let child_size = child.get().draw_frame(
                (parent.0 + self.x, parent.1 + self.y + y_offset as i32),
                draw,
                theme,
            );
            largest_x = largest_x.max(child_size.0);
            y_offset += child_size.1;
        }

        (largest_x, y_offset)
    }

    fn handle_event(&mut self, event: crate::input::InputEvent) {
        for child in &self.children {
            child.get().handle_event(event);
        }
    }
}

#[derive(Default, Clone)]
pub struct HStack {
    x: ComponentCoordiate,
    y: ComponentCoordiate,
    pub children: Vec<Shared<dyn Component>>,
}

impl HStack {
    pub fn new(x: ComponentCoordiate, y: ComponentCoordiate) -> Self {
        Self {
            x,
            y,
            children: Vec::new(),
        }
    }

    pub fn child<T: Component + 'static>(mut self, child: Shared<T>) -> Self {
        self.children.push(child);
        self
    }
}

impl Component for HStack {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        let mut largest_y = 0u32;
        let mut x_offset = 0u32;
        for child in &self.children {
            let child_size = child.get().draw_frame(
                (parent.0 + x_offset as i32 + self.x, parent.1 + self.y),
                draw,
                theme,
            );
            largest_y = largest_y.max(child_size.1);
            x_offset += child_size.0;
        }

        (x_offset, largest_y)
    }

    fn handle_event(&mut self, event: crate::input::InputEvent) {
        for child in &self.children {
            child.get().handle_event(event);
        }
    }
}

#[derive(Default, Clone)]
pub struct ZStack {
    x: ComponentCoordiate,
    y: ComponentCoordiate,
    pub children: Vec<Shared<dyn Component>>,
}

impl ZStack {
    pub fn new(x: ComponentCoordiate, y: ComponentCoordiate) -> Self {
        Self {
            x,
            y,
            children: Vec::new(),
        }
    }

    pub fn child<T: Component + 'static>(mut self, child: Shared<T>) -> Self {
        self.children.push(child);
        self
    }
}

impl Component for ZStack {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        let mut largest_y = 0u32;
        let mut largest_x = 0u32;
        for child in &self.children {
            let child_size =
                child
                    .get()
                    .draw_frame((parent.0 + self.x, parent.1 + self.y), draw, theme);
            largest_x = largest_x.max(child_size.0);
            largest_y = largest_y.max(child_size.1);
        }

        (largest_x, largest_y)
    }

    fn handle_event(&mut self, event: crate::input::InputEvent) {
        for child in &self.children {
            child.get().handle_event(event);
        }
    }
}
