use nx::mem::Shared;

use crate::{colour::Colour, vstack};

use super::{
    rectangle::ColouredRectangle, stack::VStack, Component, ComponentCoordiate, ComponentSize,
};

#[derive(Clone)]
pub struct PrideFlag {
    vstack: Shared<VStack>,
}

impl PrideFlag {
    pub fn new_rainbow(
        x: ComponentCoordiate,
        y: ComponentCoordiate,
        seg_width: ComponentSize,
        seg_height: ComponentSize,
    ) -> Self {
        Self {
            vstack: vstack!(
                (x, y),
                [
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(228, 3, 3, 0xFF)
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(255, 140, 0, 0xFF)
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(255, 237, 0, 0xFF)
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0, 128, 38, 0xFF)
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(36, 64, 142, 0xFF)
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(115, 41, 130, 0xFF)
                    ))
                ]
            ),
        }
    }

    pub fn new_trans(
        x: ComponentCoordiate,
        y: ComponentCoordiate,
        seg_width: ComponentSize,
        seg_height: ComponentSize,
    ) -> Self {
        Self {
            vstack: vstack!(
                (x, y),
                [
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0x55, 0xCD, 0xFC, 0xFF),
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0xFF, 0xAA, 0xBB, 0xFF),
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0xFF, 0xFF, 0xFF, 0xFF),
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0xFF, 0xAA, 0xBB, 0xFF),
                    )),
                    Shared::new(ColouredRectangle::new(
                        0,
                        0,
                        seg_width,
                        seg_height,
                        Colour::new(0x55, 0xCD, 0xFC, 0xFF),
                    ))
                ]
            ),
        }
    }
}

impl Component for PrideFlag {
    fn x(&self) -> ComponentCoordiate {
        self.vstack.get().x()
    }

    fn y(&self) -> ComponentCoordiate {
        self.vstack.get().y()
    }

    fn draw_frame(
        &mut self,
        parent: (ComponentCoordiate, ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (ComponentSize, ComponentSize) {
        self.vstack.get().draw_frame(parent, draw, theme)
    }
}
