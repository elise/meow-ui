use alloc::string::String;
use nx::mem::Shared;

use super::Component;

#[derive(Debug, Clone, Copy)]
pub enum TextSize {
    Small = 32,
    Medium = 42,
    Large = 64,
}

pub struct Text {
    pub contents: Shared<dyn TextContent>,
    pub x: i32,
    pub y: i32,
    pub size: TextSize,
    pub selected: bool,
}

impl Text {
    pub fn new<T: TextContent + 'static>(
        contents: T,
        x: i32,
        y: i32,
        size: TextSize,
        selected: bool,
    ) -> Self {
        Self {
            contents: Shared::new(contents),
            x,
            y,
            size,
            selected,
        }
    }

    pub fn new_shared(
        contents: Shared<dyn TextContent>,
        x: i32,
        y: i32,
        size: TextSize,
        selected: bool,
    ) -> Self {
        Self {
            contents,
            x,
            y,
            size,
            selected,
        }
    }

    pub fn set_content<T: TextContent + 'static>(&mut self, contents: T) {
        self.contents = Shared::new(contents);
    }
}

impl Component for Text {
    fn x(&self) -> super::ComponentCoordiate {
        self.x
    }

    fn y(&self) -> super::ComponentCoordiate {
        self.y
    }

    fn draw_frame(
        &mut self,
        parent: (super::ComponentCoordiate, super::ComponentCoordiate),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (super::ComponentSize, super::ComponentSize) {
        draw.text(
            parent.0 + self.x,
            parent.1 + self.y,
            self.size as u32 as f32,
            match self.selected {
                true => theme.selected,
                false => theme.foreground,
            },
            self.contents.get().as_str(),
        )
    }
}

pub trait TextContent {
    fn as_str(&self) -> &str;
}

impl TextContent for &'static str {
    fn as_str(&self) -> &str {
        self
    }
}

impl TextContent for String {
    fn as_str(&self) -> &str {
        self.as_str()
    }
}
