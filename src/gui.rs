use alloc::vec::Vec;
use nx::result::Result;

use crate::{draw::Draw, input::InputContext, scene::Scene, themes::Theme};

pub struct Gui {
    theme: Theme,

    surface: nx::gpu::surface::Surface,
    draw: Draw,
    input_context: InputContext,

    scenes: Vec<nx::mem::Shared<dyn Scene>>,
    scene_idx: usize,
}

impl Gui {
    pub fn new(theme: Theme, surface: nx::gpu::surface::Surface) -> Result<Self> {
        let draw = Draw::from(&surface);
        Ok(Gui {
            theme,
            surface,
            draw,
            input_context: InputContext::new()?,
            scenes: Vec::new(),
            scene_idx: 0,
        })
    }

    pub fn poll_events(&mut self) {
        let down_events = self.input_context.poll_events_down();
        let up_events = self.input_context.poll_events_up();

        if let Some(scene) = self.scenes.get(self.scene_idx) {
            for event in down_events {
                scene.get().handle_event_down(event);
            }

            for event in up_events {
                scene.get().handle_event_up(event);
            }
        }
    }

    pub fn present(&mut self) -> Result<()> {
        self.draw.start_cycle(&mut self.surface)?;
        self.draw.clear(self.theme.background);

        if let Some(scene) = self.scenes.get(self.scene_idx) {
            scene
                .get()
                .component()
                .get()
                .draw_frame((0, 0), &mut self.draw, &self.theme);
        }

        self.draw.end_cycle(&mut self.surface)?;

        Ok(())
    }

    pub fn push_scene<T: Scene + 'static>(&mut self, t: T) {
        self.scenes.push(nx::mem::Shared::new(t));
    }
}
