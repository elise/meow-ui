use alloc::boxed::Box;

use crate::{draw::Draw, input::InputEvent, themes::Theme};

pub mod button;
pub mod list;
pub mod padding;
pub mod pride;
pub mod rectangle;
pub mod stack;
pub mod text;

pub type ComponentCoordiate = i32;
pub type ComponentSize = u32;

pub struct Callback(Box<dyn Fn()>);

impl<T: Fn() + 'static> From<T> for Callback {
    fn from(value: T) -> Self {
        Callback::new(value)
    }
}

impl Callback {
    pub fn new<T: Fn() + 'static>(callback: T) -> Self {
        Self(Box::new(callback))
    }

    pub fn call(&self) {
        (self.0)();
    }
}

pub trait Component {
    fn x(&self) -> ComponentCoordiate;
    fn y(&self) -> ComponentCoordiate;

    fn handle_event(&mut self, _event: InputEvent) {}

    fn selectable(&self) -> bool {
        false
    }
    fn selected(&mut self, _selected: bool) {}

    fn draw_frame(
        &mut self,
        parent: (ComponentCoordiate, ComponentCoordiate),
        draw: &mut Draw,
        theme: &Theme,
    ) -> (ComponentSize, ComponentSize);
}
