use crate::colour::Colour;

#[derive(Clone, Copy)]
pub struct Theme {
    /// Background colour
    pub background: Colour,
    /// Foreground colour
    pub foreground: Colour,
    /// Colour for selection pointer
    pub selection: Colour,
    /// Colour for primary component in selected buttons
    pub selected: Colour,
}

impl Theme {
    pub const LIGHT: Theme = Theme {
        background: Colour::WHITE,
        foreground: Colour::BLACK,
        selection: Colour::GREEN,
        selected: Colour::BLUE,
    };

    pub const DARK: Theme = Theme {
        background: Colour::BLACK,
        foreground: Colour::WHITE,
        selection: Colour::BLUE,
        selected: Colour::GREEN,
    };
}
