pub struct NamedFont {
    pub name: &'static str,
    pub font: &'static rusttype::Font<'static>,
}

impl NamedFont {
    pub fn new(name: &'static str, font: &'static rusttype::Font<'static>) -> Self {
        Self { name, font }
    }
}
