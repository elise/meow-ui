use alloc::string::String;
use nx::mem::Shared;

use crate::{
    component::{
        rectangle::FilledRectangle,
        text::{Text, TextSize},
        Component,
    },
    hstack,
    input::InputEvent,
    vstack, zstack,
};

use super::Scene;

pub struct ButtonState {
    pub text: Shared<Text>,
    pub state: bool,
}

impl ButtonState {
    pub fn new(text: &str) -> Self {
        Self {
            text: Shared::new(Text::new(String::from(text), 0, 0, TextSize::Small, false)),
            state: false,
        }
    }
}

impl Component for ButtonState {
    fn x(&self) -> crate::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> crate::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            crate::component::ComponentCoordiate,
            crate::component::ComponentCoordiate,
        ),
        draw: &mut crate::draw::Draw,
        theme: &crate::themes::Theme,
    ) -> (
        crate::component::ComponentSize,
        crate::component::ComponentSize,
    ) {
        hstack![
            self.text.clone(),
            Shared::new(Text::new(
                String::from(match self.state {
                    true => "On",
                    false => "Off",
                }),
                0,
                0,
                TextSize::Small,
                false
            ))
        ]
        .get()
        .draw_frame(parent, draw, theme)
    }
}

pub struct InputDebug {
    // pub title: String,
    title: Shared<Text>,
    top_rect: Shared<FilledRectangle>,
    bottom_rect: Shared<FilledRectangle>,

    a_state: Shared<ButtonState>,
    b_state: Shared<ButtonState>,
    x_state: Shared<ButtonState>,
    y_state: Shared<ButtonState>,
}

impl InputDebug {
    pub fn new() -> Self {
        Self {
            top_rect: Shared::new(FilledRectangle::new(32, 72, 1216, 4, true)),
            bottom_rect: Shared::new(FilledRectangle::new(32, 720 - 72, 1216, 4, true)),
            title: Shared::new(Text::new(
                String::from("Meow! Input Debug"),
                32,
                16,
                TextSize::Medium,
                false,
            )),
            a_state: Shared::new(ButtonState::new("A: ")),
            b_state: Shared::new(ButtonState::new("B: ")),
            x_state: Shared::new(ButtonState::new("X: ")),
            y_state: Shared::new(ButtonState::new("Y: ")),
        }
    }
}

impl Default for InputDebug {
    fn default() -> Self {
        Self::new()
    }
}

impl Scene for InputDebug {
    fn component(&mut self) -> Shared<dyn Component> {
        zstack![
            self.title.clone(),
            self.top_rect.clone(),
            self.bottom_rect.clone(),
            vstack!(
                (32, 128),
                [
                    hstack![self.a_state.clone(), self.b_state.clone()],
                    hstack![self.x_state.clone(), self.y_state.clone()]
                ]
            )
        ]
    }

    fn handle_event_down(&mut self, event: crate::input::InputEvent) {
        match event {
            InputEvent::A => self.a_state.get().state = true,
            InputEvent::B => self.b_state.get().state = true,
            InputEvent::X => self.x_state.get().state = true,
            InputEvent::Y => self.y_state.get().state = true,

            _ => {}
        }
    }

    fn handle_event_up(&mut self, event: InputEvent) {
        match event {
            InputEvent::A => self.a_state.get().state = false,
            InputEvent::B => self.b_state.get().state = false,
            InputEvent::X => self.x_state.get().state = false,
            InputEvent::Y => self.y_state.get().state = false,

            _ => {}
        }
    }
}
