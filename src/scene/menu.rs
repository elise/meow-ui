use nx::mem::Shared;

use crate::{
    component::{
        padding::Padding,
        pride::PrideFlag,
        rectangle::FilledRectangle,
        stack::HStack,
        text::{Text, TextContent, TextSize},
        Component,
    },
    hstack, zstack,
};

use super::Scene;

pub struct Menu {
    pub title: Shared<Text>,
    top_rect: Shared<FilledRectangle>,
    bottom_rect: Shared<FilledRectangle>,
    pub child: Shared<dyn Component>,

    flags: Shared<HStack>,
}

impl Menu {
    pub fn new_shared_title(title: Shared<Text>, child: Shared<dyn Component>) -> Self {
        {
            let title = title.get();
            title.x = 32;
            title.y = 24;
        }

        Self {
            top_rect: Shared::new(FilledRectangle::new(32, 80, 1216, 4, true)),
            bottom_rect: Shared::new(FilledRectangle::new(32, 720 - 80, 1216, 4, true)),
            child,
            title,
            flags: hstack!(
                (32, 720 - 67),
                [
                    Shared::new(PrideFlag::new_rainbow(0, 0, 96, 10,)),
                    Shared::new(Padding(16, 1)),
                    Shared::new(PrideFlag::new_trans(0, 0, 96, 12,))
                ]
            ),
        }
    }

    pub fn new<T: TextContent + 'static>(title: T, child: Shared<dyn Component>) -> Self {
        Self::new_shared_title(
            Shared::new(Text::new(title, 32, 16, TextSize::Medium, false)),
            child,
        )
    }
}

impl Scene for Menu {
    fn component(&mut self) -> Shared<dyn Component> {
        zstack![
            zstack!((32, 96), [self.child.clone()]),
            self.title.clone(),
            self.top_rect.clone(),
            self.bottom_rect.clone(),
            self.flags.clone()
        ]
    }

    fn handle_event_down(&mut self, event: crate::input::InputEvent) {
        self.child.get().handle_event(event);
    }

    fn handle_event_up(&mut self, _event: crate::input::InputEvent) {}
}
