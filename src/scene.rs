pub mod input_debug;
pub mod menu;

use crate::{component::Component, input::InputEvent};

pub trait Scene {
    fn handle_event_down(&mut self, event: InputEvent);
    fn handle_event_up(&mut self, event: InputEvent);
    fn component(&mut self) -> nx::mem::Shared<dyn Component>;
}
