# Meow! UI

A Rusty UI Framework for Switch Homebrew

## Features

* Relative Positioning
* HOS-inspired UI

## License

This is licensed under MIT, the license is in [LICENSE](./LICENSE)

## Credits

This framework is loosely inspired by [ui2d](https://github.com/aarch64-switch-rs/ui2d/), borrowing a small bit of code in a few places

## Example

```rs
let mut gpu_ctx = gpu::Context::new(
    gpu::NvDrvServiceKind::Applet,
    gpu::ViServiceKind::Manager,
    0x40000,
)?;

let mut gui = Gui::new(
    Theme::DARK,
    gpu_ctx.create_stray_layer_surface(
        "Default",
        2,
        gpu::ColorFormat::A8B8G8R8,
        gpu::PixelFormat::RGBA_8888,
        gpu::Layout::BlockLinear,
    )?,
)?;

let rect1 = Shared::new(ColouredRectangle::new(128, 0, 128, 128, Colour::RED));
let rect2 = Shared::new(ColouredRectangle::new(228, 100, 128, 128, Colour::YELLOW));
let rect3 = Shared::new(ColouredRectangle::new(328, 200, 128, 128, Colour::GREEN));

let r1c1 = rect1.clone();
let r2c1 = rect2.clone();
let r3c1 = rect3.clone();

let r1c2 = rect1.clone();
let r2c2 = rect2.clone();
let r3c2 = rect3.clone();

let r1c3 = rect1.clone();
let r2c3 = rect2.clone();
let r3c3 = rect3.clone();

let menu_scene = Menu::new("Meow! UI", hstack![
    Shared::new(
        List::new(0, 0)
            .child(TextButton::new("List Button 1", 0, 0).on_select(move || {
                r1c1.get().colour = Colour::YELLOW;
                r2c1.get().colour = Colour::GREEN;
                r3c1.get().colour = Colour::RED;
            }))
            .child(TextButton::new("List Button 2", 0, 0).on_select(move || {
                r1c2.get().colour = Colour::RED;
                r2c2.get().colour = Colour::YELLOW;
                r3c2.get().colour = Colour::GREEN;
            }))
            .child(TextButton::new("List Button 3", 0, 0).on_select(move || {
                r1c3.get().colour = Colour::GREEN;
                r2c3.get().colour = Colour::RED;
                r3c3.get().colour = Colour::YELLOW;
            }))
            .selected_idx(1)
    ),
    zstack![
        rect1,
        rect2,
        rect3
    ]
]);

gui.push_scene(menu_scene);

loop {
    gui.present()?;
    gui.poll_events();

    // Sleep 10ms (aka 10'000'000 ns)
    svc::sleep_thread(10_000_000)?;
}
```
